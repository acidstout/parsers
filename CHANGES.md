#2024-04-30
- Added path to init script in case a Python script changes the folder for whatever reason.
- Get folder of VBScript file to eliminate hardcoded path.

#2023-03-12
- Scott Adams decided to shut down Dilbert, so it has been removed from the init script.

#2022-11-17
- Updated Maloney parser to work with latest changes on the SRF website.

#2022-05-19
- Happy 40th birthday, my beloved brother! :)
- Updated xkcd parser to properly check for existing files.
- Added deprecation notice to Cyanide & Happiness parser. For now this parser does not work, and I don't know when I have the time and inclination to do it.

#2021-12-14
- Updated Dilbert perser init script in order to put the comic stips into folders by year.