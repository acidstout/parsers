XKCD Parser
=======================
Simple script to download the XKCD comic strips.


Command line parameters
=======================
-o, --output	Folder where to put the downloaded comic. Defaults to current folder if omitted.


Known issues
=======================
None so far. If you find one, create an issue for it.


Requirements
=======================
Python 3 and urllib.

