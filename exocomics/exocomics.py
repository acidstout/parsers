#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
#
# exocomics RSS image feed parser
# @author: nrekow
# @version 0.1.1
#
# Parses the RSS feed and creates a Bash/Batch script with wget
# entries for each image. Images will be stored using the respective
# title as found in the parsed RSS feed.
#
#
# ---------------------------------------------------------------------------

from __future__ import print_function

from lxml import etree

import os
import glob
import re
import sys
import argparse
import requests


if sys.version_info[0] <= 2:
	print('This script requires Python 3 to run.')
	sys.exit(0)


# For catching URLError while trying to download comic strip
try:
	from urllib.error import URLError
	import urllib.request as ul
except ImportError:
	print('This script requires the urllib module to be installed.')
	sys.exit(0)


def main():
	print('exocomics Parser')
	# May be used to create a log file in the script's folder. Currently not in use.
	#script_path = os.path.abspath(os.path.dirname(__file__))

	args = parse_input_arguments()

	# If a dump folder has been defined,
	# create it (if does not already exists)
	# and move into it.
	try:
		if args.output != '.' and not(os.path.isdir(args.output)):
			os.makedirs(args.output)
	except OSError:
		args.output = '.'
	
	os.chdir(args.output)

	try:
		# The URL of the RSS feed
		url = 'https://www.exocomics.com/index.xml'
	
		comics = glob.glob('./*')
		comics_filenames = []
		comics_blacklist = [14, 410] # Non-existing comics.
	
		for comic in comics:
			base = os.path.basename(comic)
			comics_filenames.append(os.path.splitext(base)[0])
	
		# Fetch RSS feed
		try:
			# Creat a session. This is important, because otherwise we get error 403.
			session = requests.Session()
	
			# Fetch RSS feed
			response = session.get(url, headers={
				'User-Agent': 'Mozilla/5.0'
			})
	
			html = response.content
			# print(html)
			
			content = etree.fromstring(html)
			
			# Parse RSS feed
			# print('Entering loop 1')
			for item in content.xpath('/rss/channel/item'):
				# Get title of image.
				title = item.xpath("./title/text()")[0]
			
				# Cleanup title, because we'll use it as filename.
				# Keep these characters and replace everything else but a-zA-Z0-9.
				keepcharacters = (' ', '.', ',', '-', '_', '&', '\'', u'Ä', u'Ö', u'Ü', u'ä', u'ö', u'ü', u'ß')
				title = "".join([c for c in title if re.match(r'\w', c) or c in keepcharacters])
				
				# Remove leading and trailing whitespaces.
				title = title.strip()
	
				# Set file extension.
				ext = '.jpg'
				
				# print(title + ', ' + link)
				# print('Entering loop 2')
				for i in range(1, int(title) + 1):
					n = str(i)
					
					# Add leading zero if < 10
					if i < 10:
						n = '0' + n
					
					comic_file = n + ext
	
					if i in comics_blacklist:
						# print('Blacklisted: ' + comic_file)
						continue
					
					if n not in comics_filenames:
						print('Trying to download: ' + comic_file + ' ... ', end='')
						comic_response = session.get('https://exocomics.com/' + n + '/' + comic_file, headers={
							'User-Agent': 'Mozilla/5.0'
						})
						
						if comic_response.status_code == 200:
							print('ok')
							try:
								fd = open(comic_file, 'wb')
								fd.write(comic_response.content)
								fd.close()
							except:
								print('Cannot write to file!')
						else:
							print(comic_response.reason + ' (HTTP ' + str(comic_response.status_code) + ')')
				
				# We just need the first item, which is the last comic.
				break
	
		except URLError as e:
			print('failed with error', e.code, 'while trying to download ', url)
			sys.exit(0)

	except (KeyboardInterrupt, SystemExit):
		print('User requested program exit.')
		sys.exit(1)


# Parse input arguments if any.
def parse_input_arguments():
	argp = argparse.ArgumentParser(description = 'exocomics Parser. Script to download exocomic strips.')
	argp.add_argument('-o', '--output',
		dest = 'output',
		help = 'Comics dump folder',
		default = '.')
	args = argp.parse_args()
	return args

			
if __name__ == '__main__':
	main()
