Collection of different parsers
===============================
This is a collection of different parsers I wrote. Each folder contains at least one parser. See their readme file for details.

Installation
===============================
I set up this init script to execute the actual parser from its respective folder and download into a subfolder with the name of the parser. By default it uses the current folder where this init script resides. You may want to change that.