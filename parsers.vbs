'
' Used to start the init script without a window.
' Assumes that init script is in the same folder.
'
' @author nrekow
' @version 0.1
'
scriptDir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
CreateObject("WScript.Shell").Run Chr(34) & scriptDir & "\parsers.cmd" & Chr(34), 0, true