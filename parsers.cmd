@echo off
::
:: Windows Cmd script to run all parsers one after another.
::
:: @version 1.1.2
:: @author nrekow
::
setlocal
:: Get dir of this script.
set SCRIPTPATH=%~dp0

:: Strip trailing backslash.
set DESTINATION=%SCRIPTPATH:~0,-1%

:: Get current year. This depends on the system's locale and must be changed
:: for locales other than German.
set %DATE%=date /t
set YEAR=%DATE:~6,4%

:: Change into destination dir (e.g., dir of this script).
cd /d %DESTINATION%

:: Fetch all clips, but Dilbert and Philipp Malony, because those are handled different.
for %%P in (alternativlos,exocomics,nauticradio,xkcd) do (py %DESTINATION%\%%P\%%P.py -o %DESTINATION%\%%P)

:: Fetch Dilbert clips and separate them into folders by years.
:: Obsolete since 2023-03-12. See CHANGES.md for details.
:: py %DESTINATION%\dilbert.py -o %DESTINATION%\dilbert\%YEAR%

:: Fetch Philipp Malony clips.
cd /d %DESTINATION%\maloney
call maloney.cmd
endlocal