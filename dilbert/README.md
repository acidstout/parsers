Useless
=======================
Scott Adams decided to take down all comic strips as of March, 12th 2023. So, this parser is useless as there are no longer no comics to download. He moved to Twitter/X and some other platform where you need to pay to see his strips.


Dilbert Parser
=======================
Simple script to download the Dilbert comic strips in a defined period of time. If no arguments are passed to the script, it will download all the Dilbert comic strips of the current yeary into the current folder. The first strip appeared on April, 17th 1989. The last strip appeared on March, 12th 2023. Since then on all comic strips disappeared, so this parser is rather useless.


Command line parameters
=======================
-s, --start		Date of first comic to download. Defaults to Jan 1st of the current year. That makes it possible to separate the strips into folders by year using a small cmd/Bash script, which calls this Python script. :)

-e, --end		Date of last comic to download. Defaults to today.

-o, --output	Folder where to put the downloaded comic. Defaults to current folder.


Known issues
=======================
Sometimes the connection to the server is blocked or canceled. In that case the script waits another 10 seconds and retries one more time. If that also fails the download of the current strip is skipped and should be fetched manually by running the script again some time later. There is nothing I can do about it, anbd I don't want to increase the wait duration any further as it's still quite high.


Requirements
=======================
This script requires at least Python 3 to run. Also make sure to have the following modules installed:
- dateutil (install with "python -m pip install python-dateutil")
- imghdr
- urllib
besides the system's default modules.


Acknowledgement
=======================
Based on the work of Alvaro (apadi) @ https://gist.github.com/apadi/d5a12a301b318397a7ed
