@echo off & setlocal
::
:: Simple script to trigger child-scripts.
::
:: @author: nrekow
::
set SCRIPTPATH=%~dp0
cd %SCRIPTPATH:~0,-1%
if exist %SCRIPTPATH%\maloney.php (
	echo Checking for new Maloney episodes ...
	start /d %SCRIPTPATH% /wait /b php -f maloney.php
	echo done.
	if exist %SCRIPTPATH%\maloney_download.cmd (
		echo Downloading episodes ...
		:: Execute and wait for completion. Then delete the file.
		call %SCRIPTPATH%\maloney_download.cmd
		del %SCRIPTPATH%\maloney_download.cmd
		echo done.
	) else (
		echo No new episodes found.
	)
) else (
	echo This script requires maloney.php which is missing. Aborted.
)
endlocal