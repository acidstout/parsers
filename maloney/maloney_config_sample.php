<?php
// If true, a Bash/Batch script with wget commands will be created.
define('CREATE_LIST_ONLY', true);

// If true, the generated script will be run after its creation.
define('RUN_SCRIPT', false);

// User-agent to send to the web-server. You should use a common one for maximum compatibility.
define('USER_AGENT', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36');

// Default destination for the MP3 files is a subfolder named "maloney" in the current folder.
define('DESTINATION_FOLDER', __DIR__ . DIRECTORY_SEPARATOR . 'maloney');

// Default destination file for the temporary download script. No need to change it. If you change it, you need to update the respective Bash/Batch file as well.
define('DESTINATION_LIST_FILE', __DIR__ . DIRECTORY_SEPARATOR . 'maloney_download');

// Enable/disable notification via ntfy.sh. If you set this to TRUE, you also need to set NOTIFYURL to your respective ntfy.sh account.
define('NOTIFY', false);

// ntfy.sh account URI.
define('NOTIFYURL', 'https://ntfy.sh/<your account>');

// Currently, only the first three pages contain downloads.
define('MALONEY_MAX_PAGES', 3);

// Set a log file, because you want to know where it fails if it fails.
define('DEBUG_FILE',  realpath(dirname(__FILE__, 3)) . DIRECTORY_SEPARATOR . 'maloney.log');
