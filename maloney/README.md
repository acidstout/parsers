Philip Maloney Parser
=======================
Simple script to download episodes of "Die haarsträubenden Fälle des Philip Maloney" from SRF.


Usage
=======================
1. Make sure requirements are met.
2. Rename `maloney_config_sample.php` to `maloney_config.php` and set options to you likings.
3. Run maloney.cmd or maloney.sh (don't forget to make it executable first e.g., `chmod +x maloney.sh`).
4. Grab a coffee as downloading will take some time.
5. Consider setting up a cronjob/task which runs this script once a week to have it download new episodes automatically.


Requirements
=======================
The script requires the following dependencies to be installed:
- php-cli
- php-curl
- wget

The script should run out of the box as long as you have the requirements installed and run it from a native Linux or Windows system. macOS should also work fine, but I cannot test it, because I don't like gnawed fruits.

If you wish to run it from WSL, you will need to install the libxml2-utils in addition to the requirements.


Known issues
=======================
None so far until SRF updates their website again.
