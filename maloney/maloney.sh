#!/bin/bash
#
# Simple script to trigger child-scripts.
#
if [ -f ./maloney.php ]; then
	echo -n 'Checking for new Maloney episodes ... '
	php -f ./maloney.php
	echo 'done.'
	
	if [ -f ./maloney_download.sh ]; then
		echo -n 'Downloading episodes ... '
		chmod +x ./maloney_download.sh
		
		# Execute and wait for completion. Then delete the file.
		$(./maloney_download.sh)
		rm ./maloney_download.sh
		echo 'done.'
	else
		echo 'No new episodes found.'
	fi
else
	echo 'This script requires maloney.php which is missing. Aborted.'
fi
