<?php
/**
 * Maloney parser
 *
 * This is an implementation in PHP using cURL and wget.
 *
 * Due to the nature of PHP this is not recommended for regular use.
 * Trying to download multiple files will likely result in a timeout
 * or in a "memory exhaust" error.
 *
 * For now, this creates a Bash or Batch file (depending on the OS
 * in use) which downloads all available episodes which do not
 * exist in the maloney sub-folder of this script.
 *
 * That way resource usage is reduced to the bare minimum and the
 * script does not run into PHP timeout issues.
 *
 * Anyway, I really would like to rewrite this in Python in order to
 * get rid of those limitations and dependencies.
 *
 * @author nrekow
 * @version 1.3.5
 */

// Try to get configuration from file.
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'maloney_config.php')) {
	require_once 'maloney_config.php';
} else {
	echo 'No configuration file found. Falling back to defaults.';
}

// Check if configuration has been set and otherwise define fallback options.
if (!defined('CREATE_LIST_ONLY')) define('CREATE_LIST_ONLY', true);
if (!defined('RUN_SCRIPT')) define('RUN_SCRIPT', false);
if (!defined('USER_AGENT')) define('USER_AGENT', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36');
if (!defined('DESTINATION_FOLDER')) define('DESTINATION_FOLDER', __DIR__ . DIRECTORY_SEPARATOR . 'maloney');
if (!defined('DESTINATION_LIST_FILE')) define('DESTINATION_LIST_FILE', __DIR__ . DIRECTORY_SEPARATOR . 'maloney_download');
if (!defined('MALONEY_MAX_PAGES')) define('MALONEY_MAX_PAGES', 3);
if (!defined('NOTIFY')) define('NOTIFY', false);
if ((!defined('NOTIFYURL') || empty(str_replace('https://ntfy.sh/', '', NOTIFYURL))) && NOTIFY === true) echo 'Invalid ntfy.sh URI in configuration. Falling back to default PHP log.';
if (!defined('DEBUG_FILE')) define('DEBUG_FILE',  __DIR__ . DIRECTORY_SEPARATOR . 'maloney.log');


// Check if this is run against PHP-CLI.
if (strtoupper(substr(php_sapi_name(), 0, 3)) != 'CLI' && !function_exists('cli_set_process_title')) {
	echo 'This script must be run from PHP-CLI.';
	die();
}

// Check if cURL extension is enabled.
if (!extension_loaded('curl')) {
	echo 'This script requires the cURL extension to be enabled.';
	die();
}

// Backward compatibility fix.
if (!defined('PHP_EOL')) {
	switch (strtoupper(substr(PHP_OS, 0, 3))) {
		// Windows
		case 'WIN':
			define('PHP_EOL', "\r\n");
			break;
			
			// Mac
		case 'DAR':
			define('PHP_EOL', "\r");
			break;
			
			// Unix
		default:
			define('PHP_EOL', "\n");
	}
}

// Disable execution time limit.
set_time_limit(0);


/**
 * Log messages
 *
 * @param string $msg
 * @param boolean $is_fatal
 */
function log_msg($msg, $is_fatal = false) {
	$msg = '[' . date('Y-m-d H:i:s') . '] ' . $msg;
	error_log($msg . "\n", 3, DEBUG_FILE);
	if ($is_fatal !== false) {
		die();
	}
}


/**
 * Get a list of already downloaded episodes.
 *
 * @return array
 */
function getLocalEpisodes() {
	$existingEpisodes = array();
	foreach (array_filter(glob(DESTINATION_FOLDER . '/*.mp3'), 'is_file') as $file) {
		$existingEpisodes[] = $file;
	}
	
	return $existingEpisodes;
}


/**
 * Get episode number by title.
 *
 * @param string $episodeTitle
 * @return string
 */
function getEpisodeNumberByTitle($episodeTitle) {
	$episodes = array(
			'0001' => 'Die kleine Schwester',
			'0002' => 'Der grosse Schlaf',
			'0003' => 'Das hohe Fenster',
			'0004' => 'Der lange Abschied',
			'0005' => 'Playback',
			'0006' => 'Die Dame im See',
			'0007' => 'Lebwohl, mein Liebling',
			'0008' => 'Die letzte Fahrt',
			'0009' => 'Die schwarze Katze',
			'0010' => 'Der verlorene Sohn',
			'0011' => 'Wien bei Nacht',
			'0012' => 'Die alten Griechen',
			'0013' => 'Der zweite Mann',
			'0014' => 'Das achte Loch',
			'0015' => 'Der neue Einstein',
			'0016' => 'Hopfen und Malz',
			'0017' => 'Schwarz auf Weiss',
			'0018' => 'Jackpot',
			'0019' => 'Full-House',
			'0020' => 'Der Blockwart',
			'0021' => 'Ein todsicheres Geschäft',
			'0022' => 'Das Versprechen',
			'0023' => 'Hasta Luego',
			'0024' => 'Die Verfolgte',
			'0025' => 'Die Fahrt ins Blaue',
			'0026' => 'Der Morgen danach',
			'0027' => 'Der Aussteiger',
			'0028' => 'Der grosse Bruder',
			'0029' => 'Der Lift',
			'0030' => 'Tod in Hollywood',
			'0031' => 'Das Ohr des Meisters',
			'0032' => 'Die Brieftaube',
			'0033' => 'Die andere Frau',
			'0034' => 'Der schwarze Koffer',
			'0035' => 'Der Swimmingpool',
			'0036' => 'Die Blumen des Bösen',
			'0037' => 'Der Zwillingsbruder',
			'0038' => 'Die Rechnung aus dem Jenseits',
			'0039' => 'Der Spaziergänger',
			'0040' => 'Der Knall',
			'0041' => 'Seltsame Vögel',
			'0042' => 'Mord nach Noten',
			'0043' => 'Das Muttermal',
			'0044' => 'Der Entführte',
			'0045' => 'Der Tote im Park',
			'0046' => 'Hawaii einfach',
			'0047' => 'Endstation Sehnsucht',
			'0048' => 'Schnee von Gestern',
			'0049' => 'Ein seltsames Paar',
			'0050' => 'Seemannsgarn',
			'0051' => 'Das Sommerloch',
			'0052' => 'Die Terrasse',
			'0053' => 'Die Klinik',
			'0054' => 'Das Auto',
			'0055' => 'Abgereist ohne Adressangabe',
			'0056' => 'Löcher im Käse',
			'0057' => 'Die Chaotin',
			'0058' => 'Hundeleben',
			'0059' => 'Der Killerinstinkt',
			'0060' => 'Zaubertricks',
			'0061' => 'Der Ring',
			'0062' => 'Mord im 3-4 Takt',
			'0063' => 'Schwedische Gardinen',
			'0064' => 'Der gerissene Film',
			'0065' => 'Die Laube',
			'0066' => 'Der Ernstfall',
			'0067' => 'Die nassen Hosen',
			'0068' => 'Der nackte Krieger',
			'0069' => 'Das schwache Herz',
			'0070' => 'Die Erstausgabe',
			'0071' => 'Der Bruch',
			'0072' => 'Die Liste',
			'0073' => 'Klopfzeichen',
			'0074' => 'Heisse Tage',
			'0075' => 'Die unsichtbare Leiche',
			'0076' => 'Der Anfänger',
			'0077' => 'Das Anwaltsgeheimnis',
			'0078' => 'Die Weltmeister',
			'0079' => 'Ein Mord mit Talent',
			'0080' => 'Der schöne Thomas',
			'0081' => 'Der Spieler',
			'0082' => 'Bilder lügen nicht',
			'0083' => 'Süsse Geheimnisse',
			'0084' => 'Der Stein der Weisen',
			'0085' => 'Die Armbanduhr',
			'0086' => 'Der Mörderhai',
			'0087' => 'Druckbuchstaben',
			'0088' => 'Der Wettkampf',
			'0089' => 'Das trojanische Pferd',
			'0090' => 'Familienbande',
			'0091' => 'Der Feuerteufel',
			'0092' => 'Der Vermisste',
			'0093' => 'Der Heiratsschwindler',
			'0094' => 'Die Pille',
			'0095' => 'Der Goldfisch',
			'0096' => 'Fälschungen',
			'0097' => 'Der liebe Onkel',
			'0098' => 'Die Handtasche',
			'0099' => 'Durch dick und dünn',
			'0100' => 'Die Lösung',
			'0101' => 'Tödliche Ausstellung',
			'0102' => 'Auf der Flucht',
			'0103' => 'Dunkle Geschäfte',
			'0104' => 'Schadenfreude',
			'0105' => 'Suchtgefahr',
			'0106' => 'Der Händler',
			'0107' => 'Die blaue Tasche',
			'0108' => 'Aus heiterem Himmel',
			'0109' => 'Der Postraub',
			'0110' => 'Die Spätvorstellung',
			'0111' => 'Garten des Todes',
			'0112' => 'Tod eines Baulöwen',
			'0113' => 'Das Geschäftsessen',
			'0114' => 'Zur Hölle mit Don Giovanni',
			'0115' => 'Hundstage',
			'0116' => 'Kunstfehler',
			'0117' => 'Galerie der Toten',
			'0118' => 'Das Sommerrätsel',
			'0119' => 'Das Hausfest',
			'0120' => 'Der Untermieter',
			'0121' => 'Der Doppelgänger',
			'0122' => 'Das Ende aller Sorgen',
			'0123' => 'Blind Date',
			'0124' => 'Die Nacht der langen Messer',
			'0125' => 'Das Appartement',
			'0126' => 'Die Hand',
			'0127' => 'Der grosse Knall',
			'0128' => 'Der Angler',
			'0129' => 'Das Portrait',
			'0130' => 'Lange Nächte',
			'0131' => 'Mord nach Plan',
			'0132' => 'Der Weg nach oben',
			'0133' => 'Seltsame Zeugen',
			'0134' => 'Der blaue Dunst',
			'0135' => 'Die bösen Schwestern',
			'0136' => 'Der Womper',
			'0137' => 'Schüsse im Äther',
			'0138' => 'Der direkte Draht',
			'0139' => 'Das andere Leben',
			'0140' => 'Der Ball ist rund',
			'0141' => 'Rollenspiele',
			'0142' => 'Das Rattennest',
			'0143' => 'Das Erbe',
			'0144' => 'Durch die Blume',
			'0145' => 'Schöne Bescherung',
			'0146' => 'Das stille Örtchen',
			'0147' => 'Das Jubiläum',
			'0148' => 'Die Dämonen',
			'0149' => 'Der Banküberfall',
			'0150' => 'Mord im Parkhaus',
			'0151' => 'Der schöne Fuss',
			'0152' => 'Die Bombenbastler',
			'0153' => 'Ein tierischer Verdacht',
			'0154' => 'Ein friedlicher Park',
			'0155' => 'Das Künstlerheim',
			'0156' => 'Der Grabstein',
			'0157' => 'Das Seminar',
			'0158' => 'Der Test',
			'0159' => 'Die Ferienbekanntschaft',
			'0160' => 'Die fremde Wohnung',
			'0161' => 'Das Comeback',
			'0162' => '23 Rosen',
			'0163' => 'Der Mann im Hotel',
			'0164' => 'Der Tramkiller',
			'0165' => 'Der falsche Hund',
			'0166' => 'Der Elsterverein',
			'0167' => 'Die Saisonkarte',
			'0168' => 'Der Fasnachtsmörder',
			'0169' => 'Die Mondfinsternis',
			'0170' => 'Das doppelte Leben',
			'0171' => 'Die Alters-WG',
			'0172' => 'Die Agenda',
			'0173' => 'Die Dichterlesung',
			'0174' => 'Die Bedrohung',
			'0175' => 'Mord im Theater',
			'0176' => 'Tod eines Architekten',
			'0177' => 'Der Bootsausflug',
			'0178' => 'Der Schatten',
			'0179' => 'Die Beute',
			'0180' => 'Der Geizhals',
			'0181' => 'Das Nebeneinkommen',
			'0182' => 'Der Fremde',
			'0183' => 'Zum Kuckuck',
			'0184' => 'Die Farbtherapie',
			'0185' => 'Eiszeit',
			'0186' => 'Der Höhlenmensch',
			'0187' => 'Tod im Schnee',
			'0188' => 'Tödliches Frühstück',
			'0189' => 'Das zweite Ich',
			'0190' => 'Der Bodybuilder',
			'0191' => 'Blitz und Donner',
			'0192' => 'Die Einbrecherin',
			'0193' => 'Geisterstunde',
			'0194' => 'Fanpost',
			'0195' => 'Trickdiebe',
			'0196' => 'Die Abseitsfalle',
			'0197' => 'Das Rennpferd',
			'0198' => 'Hirngespinste',
			'0199' => 'Das Investment',
			'0200' => 'Der Jungbrunnen',
			'0201' => 'Die verschwundenen Trucks',
			'0202' => 'Das Freudenhaus',
			'0203' => 'Der Baum',
			'0204' => 'Marsmenschen',
			'0205' => 'Weidmannsheil',
			'0206' => 'Nachbarschaftshilfe',
			'0207' => 'Das Skelett',
			'0208' => 'Tiefdruckgebiete',
			'0209' => 'Der Autodieb',
			'0210' => 'Der Vorleser',
			'0211' => 'Die lieben Nachbarn',
			'0212' => 'Tödliches Spiel',
			'0213' => 'Billy the Kid',
			'0214' => 'Der Raubüberfall',
			'0215' => 'Haarige Zeiten',
			'0216' => 'Der Teddybär',
			'0217' => 'Der Schlüssel',
			'0218' => 'Der Klabautermann',
			'0219' => 'Der Bestseller',
			'0220' => 'Das Feuerwerk',
			'0221' => 'Schmuckstücke',
			'0222' => 'Der Sprayer',
			'0223' => 'Das Kaffeekränzchen',
			'0224' => 'Das Erlebnishotel',
			'0225' => 'Die Schlafwandlerin',
			'0226' => 'Von Quoten und Toten',
			'0227' => 'Die Tonjäger',
			'0228' => 'Oh Tannenbaum',
			'0229' => 'Die Millenniumsleiche',
			'0230' => 'Der Hellseher',
			'0231' => 'Prüfungsangst',
			'0232' => 'Der Matador',
			'0233' => 'Die Sprengmeister',
			'0234' => 'Der Vertrag',
			'0235' => 'Flugangst',
			'0236' => 'Atemlos',
			'0237' => 'Der Schatz im Silberwald',
			'0238' => 'Die Rentnergang',
			'0239' => 'Der Traummann',
			'0240' => 'Der Ersatzmann',
			'0241' => 'Der Schneemann',
			'0242' => 'Der Auftrag',
			'0243' => 'Mord auf CD',
			'0244' => 'Der Mann mit der Maske',
			'0245' => 'Die Expedition',
			'0246' => 'In vino veritas',
			'0247' => 'Es war einmal ein Mord',
			'0248' => 'Der Fotograf',
			'0249' => 'Der rote Rucksack',
			'0250' => 'Die verschwundenen Käfer',
			'0251' => 'Der Mann ohne Alibi',
			'0252' => 'Tödlicher Chat',
			'0253' => 'Ein Schluck zu viel',
			'0254' => 'Ein seltsamer Bruder',
			'0255' => 'Brieffreundschaften',
			'0256' => 'Der Mann in der Hängematte',
			'0257' => 'Die Armbrust',
			'0258' => 'Der Schrumpfkopf',
			'0259' => 'Zahn um Zahn',
			'0260' => 'Der geheimnisvolle Schlüssel',
			'0261' => 'Der Mann auf der Brücke',
			'0262' => 'Die Tankstelle',
			'0263' => 'Die Vögel',
			'0264' => 'Der Besucher',
			'0265' => 'Stillleben mit Leiche',
			'0266' => 'Die Nichtschwimmer',
			'0267' => 'Der Regenschirm',
			'0268' => 'Der Ausserirdische',
			'0269' => 'Geboren am 1. August',
			'0270' => 'Tödliche Fragen',
			'0271' => 'Das Haar in der Suppe',
			'0272' => 'Der Traumjob',
			'0273' => 'Schwarzgeld',
			'0274' => 'Verfolgungswahn',
			'0275' => 'Der Mann am Fenster',
			'0276' => 'Der Schuh des Mörders',
			'0277' => 'Die Todesanzeige',
			'0278' => 'Der Tante Emma Laden',
			'0279' => 'Die Politesse',
			'0280' => 'Wie im Schlaf',
			'0281' => 'Viel Lärm um Nichts',
			'0282' => 'Der mörderische Reigen',
			'0283' => 'Ein armer Schlucker',
			'0284' => 'Die Putzfrau',
			'0285' => 'Der Hausierer',
			'0286' => 'Der letzte Tag',
			'0287' => 'Der Balkonmörder',
			'0288' => 'Der unsichtbare Schatz',
			'0289' => 'Die schwarzen Witwen',
			'0290' => 'Die blutigen Kochlöffel',
			'0291' => 'Tödliche Kunst',
			'0292' => 'Der Stromschlag',
			'0293' => 'Der Spanner',
			'0294' => 'Der unbekannte Tote',
			'0295' => 'Der gute Zweck',
			'0296' => 'Die gestohlene Identität',
			'0297' => 'Die wilde Drei',
			'0298' => 'Die rote Lokomotive',
			'0299' => 'Der Aprilscherz',
			'0300' => 'Der Biedermann',
			'0301' => 'Das Erbe des Herrn Kuhn',
			'0302' => 'Die Frau aus Ungarn',
			'0303' => 'Tödliches Camping',
			'0304' => 'Das Geständnis',
			'0305' => 'Gefährliche Salben',
			'0306' => 'Die Qual der Wahl',
			'0307' => 'Der letzte Deal',
			'0308' => 'Geld oder Leben',
			'0309' => 'Das geheimnisvolle Haus',
			'0310' => 'Der Koffer im Schnee',
			'0311' => 'Die Puppenmacherin',
			'0312' => 'Tödliches Dinner',
			'0313' => 'Im tiefsten Mittelalter',
			'0314' => 'Das Passwort',
			'0315' => 'Die blaue Jacke',
			'0316' => 'Der besondere Tag',
			'0317' => 'Die eiserne Reserve',
			'0318' => 'Der kleine Bruder',
			'0319' => 'Spekulationen',
			'0320' => 'Tote Liebe',
			'0321' => 'Die Stimme',
			'0322' => 'Im Schneckentempo',
			'0323' => 'Der verschwundene Nachbar',
			'0324' => 'Zerstörtes Glück',
			'0325' => 'Die Vision',
			'0326' => 'Stille Wasser',
			'0327' => 'Das zweite Standbein',
			'0328' => 'Eiseskälte',
			'0329' => 'Der Fahnder',
			'0330' => 'Der Lauscher',
			'0331' => 'Schlechtes Timing',
			'0332' => 'Jugendsünden',
			'0333' => 'Phantomschmerz',
			'0334' => 'Das Tattoo',
			'0335' => 'Ein Gruss aus Südamerika',
			'0336' => 'Die Beobachter',
			'0337' => 'Die tödliche Kurve',
			'0338' => 'Die tödliche Geschäftsidee',
			'0339' => 'Es geschah am 1. Mai',
			'0340' => 'Die Frau im Keller',
			'0341' => 'Der Jakobsweg',
			'0342' => 'Die Erpressung',
			'0343' => 'Anti-Aging',
			'0344' => 'Burn-Out',
			'0345' => 'Der Parkschein',
			'0346' => 'Das alte Haus',
			'0347' => 'Das Schneeballsystem',
			'0348' => 'Die verschwundene Frau',
			'0349' => 'Der letzte Gang',
			'0350' => 'Die Frau in Rot',
			'0351' => 'Die falsche Tochter',
			'0352' => 'Mitten in der Nacht',
			'0353' => 'Die verschwundene Tasche',
			'0354' => 'Die Urne',
			'0355' => 'Undercover',
			'0356' => 'Das rote Haus',
			'0357' => 'Der grüne Daumen',
			'0358' => 'Die Paten',
			'0359' => 'Ausser Kontrolle',
			'0360' => 'Schachmatt',
			'0361' => 'Sturmwarnung',
			'0362' => 'Das Spukhaus',
			'0363' => 'Der Plan',
			'0364' => 'Die Heilerin',
			'0365' => 'Die Stimme des Täters',
			'0366' => 'Eine schillernde Frau',
			'0367' => 'Schlank und tot',
			'0368' => 'Der Mittwochsclub',
			'0369' => 'Der richtige Zeitpunkt',
			'0370' => 'Tödliche Koordinaten',
			'0371' => 'Mietnomaden',
			'0372' => 'Tödliches Start-Up',
			'0373' => 'Die gelbe Gefahr',
			'0374' => 'Der Lieferwagen',
			'0375' => 'Die Überwachten',
			'0376' => 'Beste Freunde',
			'0377' => 'Der Schlüssel zum Erfolg',
			'0378' => 'Die Drohne',
			'0379' => 'Die perfekte Mischung',
			'0380' => 'Gesundheit',
			'0381' => 'Der Hilferuf',
			'0382' => 'Ein Funken Verstand',
			'0383' => 'Eine seltsame Erpressung',
			'0384' => 'Paranoia',
			'0385' => 'Superfood',
			'0386' => 'Das Geständnis',
			'0387' => 'Der Mann auf dem Dach',
			'0388' => 'Alles unter Kontrolle',
			'0389' => 'Eine fotogene Leiche',
			'0390' => 'Die Kochbuchkiller',
			'0391' => 'Frohe Weihnachten',
			'0392' => 'Der Mann im Zug',
			'0393' => 'Im Einkaufswahn',
			'0394' => 'Der Puppensammler',
			'0395' => 'Eine seltsame Entführung',
			'0396' => 'Der Sinn des Lebens',
			'0397' => 'Falsche Polizisten',
			'0398' => 'Die Bombendrohung',
			'0399' => 'Der intelligente Lautsprecher',
			'0400' => 'Das Jubiläumsgeschenk',
			'0401' => 'Die Verfolger',
			'0402' => 'Die Hochstaplerin',
			'0403' => 'Die neue Tankstelle',
			'0404' => 'Ein neues Leben',
			'SE01' => 'Philip Maloney und der Weihnachtsmann',
			'SE02' => 'Auf Messers Schneide',
			'SE03' => 'Der Geheimdienst'
	);
	
	$result = array_search($episodeTitle, $episodes);
	if ($result === false) {
		return '####';
	}
	return $result;
}


/**
 * Get content of a webpage.
 *
 * @param string $url
 * @return array
 */
function getPage($url, $type = 'GET', $payload = null) {
	$options = array(
			CURLOPT_CUSTOMREQUEST  => $type,		// Request type.
			CURLOPT_POST           => false,		// Yes, we'll send a GET request.
			CURLOPT_REFERER        => $url,			// Destination URL.
			CURLOPT_USERAGENT      => USER_AGENT,	// User-agent.
			CURLOPT_HEADER         => false,		// Don't return headers.
			CURLOPT_RETURNTRANSFER => true,			// But return web page.
			CURLOPT_ENCODING       => '',			// Handle all encodings. We don't care.
			CURLOPT_VERBOSE        => false,		// Hide debug messages.
			//CURLOPT_VERBOSE        => true,		// Show debug messages.
			CURLOPT_SSL_VERIFYHOST => false,		// Disable SSL/TLS verification of the host.
			CURLOPT_SSL_VERIFYPEER => false,		// Disable SSL/TLS verification of the peer.
			CURLOPT_AUTOREFERER    => true,			// Send referer on redirect.
			CURLOPT_FOLLOWLOCATION => true,			// Follow redirects.
			CURLOPT_MAXREDIRS      => 10,			// Stop after 10 redirects.
			CURLOPT_CONNECTTIMEOUT => 10,			// Timeout on connect.
			CURLOPT_TIMEOUT        => 10,			// Timeout on response.
	);
	
	if (strtoupper($type) === 'POST') {
		$options[CURLOPT_POST] = true;
		$options[CURLOPT_POSTFIELDS] = $payload;
		$options[CURLOPT_HTTPHEADER] = array('Content-Type:text/plain');
	}
	
	$ch = curl_init($url);
	curl_setopt_array($ch, $options);
	
	$content = curl_exec($ch);
	$err     = curl_errno($ch);
	$errmsg  = curl_error($ch);
	$header  = curl_getinfo($ch);
	curl_close($ch);
	
	$header['errno']   = $err;
	$header['errmsg']  = $errmsg;
	$header['content'] = $content;
	
	return $header;
}


/**
 * Get filename from path.
 * Supports multi-byte paths.
 *
 * @param string $path
 * @return string
 */
function mb_basename($path) {
	if (preg_match('@^.*[\\\\/]([^\\\\/]+)$@s', $path, $matches)) {
		return $matches[1];
	} else if (preg_match('@^([^\\\\/]+)$@s', $path, $matches)) {
		return $matches[1];
	}
	return '';
}


/**
 * Download a binary file to a given destination
 * or return a wget command-line.
 *
 * @param string $url
 * @param string $filename
 * @return string
 */
function downloadMediaFile($url, $filename) {
	$filename = DESTINATION_FOLDER . DIRECTORY_SEPARATOR . $filename;
	
	if ( ! file_exists($filename) ) {
		if (defined('NOTIFY') && NOTIFY === true) {
			$payload = 'Downloading ' . mb_basename($filename);
			if (defined('NOTIFYURL') && !empty(str_replace('https://ntfy.sh/', '', NOTIFYURL))) {
				$status = getPage(NOTIFYURL, 'POST', $payload);
			} else {
				log_msg($payload);
			}
		}
		
		if (defined('CREATE_LIST_ONLY') && CREATE_LIST_ONLY === true) {
			// wget has issues with modern SSL certificates, so we're forcing http for now.
			$url = str_replace('https:', 'http:', $url);
			// Restrict filenames, so wget does not mess around with UTF-8 characters.
			return 'wget --continue --quiet --restrict-file-names=nocontrol --output-document="' . $filename . '" ' . $url;
		} else {
			// Download file with cURL instead of generating a file.
			$fp = fopen($filename, 'w+');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			
			$response = curl_exec($ch);
			curl_close($ch);
			fclose($fp);
		}
	}
	
	return '';
}


/**
 * Main routine of the Maloney Downloader.
 */
function maloneyDownloader() {
	$entries = null;
	$episodes = null;
	$assetIds = array();
	$cmds = array();
	$i = 0; // Page number.
	$j = 0; // Counter of asset IDs.
	
	// Get a list of already downloaded episodes.
	$localEpisodes = getLocalEpisodes();
	
	// Get JSON of latest Maloney episodes.
	for ($i = 1; $i <= MALONEY_MAX_PAGES; $i++) {
		$json = getPage('https://www.srf.ch/aron/api/audio/shows/A00361/latestEpisodes?page=' . $i);
		
		if ($json['errno'] === 0) {
			$entries = $json['content'];
			
			try {
				$entries = json_decode($entries, true, JSON_OBJECT_AS_ARRAY | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_INVALID_UTF8_SUBSTITUTE | JSON_THROW_ON_ERROR);
			} catch (JsonException $e) {
				log_msg($e->getCode() . ': '. $e->getMessage());
				die();
			}
			
			// Grab assetIds from fetched JSON.
			foreach ($entries as $entry) {
				$title = preg_replace("(\r\n|\r|\n)", '', $entry['title']);
				$titleNo = getEpisodeNumberByTitle($title);
				$filename = $titleNo . ' ' . $title . '.mp3';
				
				// Create list of missing episodes.
				if ( ! in_array($filename, $localEpisodes)) {
					$assetIds[$j]['assetId'] = $entry['assetId'];
					$assetIds[$j]['filename'] = $filename;
					$j++;
				}
			}
		} else {
			log_msg('Failed to connect to SRF website.');
			die();
		}
	}
	
	// Iterate over collected asset IDs.
	foreach($assetIds as $assetId) {
		// Get another JSON and add assetId into URL to fetch information about episode.
		$json = getPage('https://il.srgssr.ch/integrationlayer/2.0/mediaComposition/byUrn/urn:srf:audio:' . $assetId['assetId'] . '.json?onlyChapters=true&vector=portalplay');
		
		if ($json['errno'] === 0) {
			// Contains result of our call.
			$episodes = $json['content'];
			$episodes = json_decode($episodes, true, JSON_OBJECT_AS_ARRAY | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_INVALID_UTF8_SUBSTITUTE | JSON_THROW_ON_ERROR);
			
			// Some episodes has URLs listed, but cannot be downloaded due to various reasons (e.g., license expired).
			// They are flagged with a block reason entry, so we can easily skip those, because we can't download them anyway.
			if (!isset($episodes['chapterList'][0]['blockReason'])) {
				$cmds[] = downloadMediaFile($episodes['chapterList'][0]['resourceList'][0]['url'], $assetId['filename']);
			}
		}
	}
	
	// Trim all array items.
	$cmds = array_map('trim', $cmds);
	
	// Remove empty items from array.
	$cmds = array_filter($cmds);
	
	// Create download script. Default to bash.
	if (defined('CREATE_LIST_ONLY') && CREATE_LIST_ONLY === true && !empty($cmds) && count($cmds) > 0) {
		$file_header = '#!/bin/bash' . PHP_EOL;
		$file_body = implode(PHP_EOL, $cmds);
		$file_footer = PHP_EOL;
		$file_ext = '.sh';
		$file_cmd = DESTINATION_LIST_FILE . $file_ext;
		
		if (defined('PHP_OS') && PHP_OS === 'WINNT') {
			// Force UTF-8 codepage on Windows, so wget does not mess up filenames.
			$file_header = '@echo off & setlocal & chcp 65001>nul' . PHP_EOL;
			$file_footer = PHP_EOL . 'endlocal' . PHP_EOL;
			$file_ext = '.cmd';
			$file_cmd = 'cmd /c ' . DESTINATION_LIST_FILE . $file_ext;
		}
		
		// Write wget commands into download script.
		$fp = null;
		$fp = fopen(DESTINATION_LIST_FILE . $file_ext, 'w');
		fwrite($fp, $file_header . $file_body . $file_footer);
		fclose($fp);
		
		// Run the generated download script.
		if (defined('RUN_SCRIPT') && RUN_SCRIPT !== false) {
			system($file_cmd);
		}
	}
}


// Let the fun begin ...
maloneyDownloader();