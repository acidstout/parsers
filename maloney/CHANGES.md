# CHANGES

## 1.3.5
- There seems to be line-breaks in the title sometimes for whatever reason, so I removed those.

## 1.3.4
- Updated check for existing episodes. Besides the asset ID, the title is already returned in the first JSON call. No need to check for existing episodes after the second call.
- Updated check of online episodes, and download all episodes which are available online, but missing locally. Some episodes cannot be downloaded due to license expiration issues. Those have a special flag and are skipped.
- Added logging.

## 1.3.3
- Added additional check, because php_sapi_name() does not reliably return information if PHP-CLI is used or not.
- Added support for multi-byte filenames.

## 1.3.2
- Updated script due to changes on SRF website.
- Added configuration file.
- Fixed hard-coded destination folder when checking for local episodes.
- Reduced string which is sent to ntfy.sh to just the filename.

## 1.3.1
- Added ntfy.sh support.

## 1.3
- Updated download and parsing functions. They return arrays now.
- Updated script creation process.
- Added checks against CLI and cURL extension.
- Added flag to run the generated script automatically afterwards.
- Disabled execution timeout, because external execution time is added to PHP execution time on Windows.
- Removed analytics date from filename. It's not relevant.
- Removed cookie file and respective cleanup. It's not relevant.
- Removed cookie cleanup from batch file. It's no longer necessary.
