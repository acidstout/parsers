#!/bin/bash
#
# Bash script to run all parsers one after another.
#
# @author: nrekow
#
DESTINATION=/mnt/e/parsers
SCRIPTPATH=$(cd $(dirname $0); pwd -P)
YEAR=$(date +"%Y")
cd $SCRIPTPATH
for i in alternativlos exocomics nauticradio xkcd; do
	python3 ./$DESTINATION/$i.py -o $DESTINATION/$i
done
python3 ./$DESTINATION/dilbert.py -o $DESTINATION/dilbert/$YEAR
./maloney.sh